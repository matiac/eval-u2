/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Matico
 */
@Entity
@Table(name = "ag_naves")
@NamedQueries({
    @NamedQuery(name = "AgNave.findAll", query = "SELECT a FROM AgNave a"),
    @NamedQuery(name = "AgNave.findByNavId", query = "SELECT a FROM AgNave a WHERE a.navId = :navId"),
    @NamedQuery(name = "AgNave.findByNavFabricante", query = "SELECT a FROM AgNave a WHERE a.navFabricante = :navFabricante"),
    @NamedQuery(name = "AgNave.findByNavModelo", query = "SELECT a FROM AgNave a WHERE a.navModelo = :navModelo"),
    @NamedQuery(name = "AgNave.findByNavConfig", query = "SELECT a FROM AgNave a WHERE a.navConfig = :navConfig"),
    @NamedQuery(name = "AgNave.findByNavMotor", query = "SELECT a FROM AgNave a WHERE a.navMotor = :navMotor"),
    @NamedQuery(name = "AgNave.findByNavEstabilizador", query = "SELECT a FROM AgNave a WHERE a.navEstabilizador = :navEstabilizador"),
    @NamedQuery(name = "AgNave.findByNavFrenado", query = "SELECT a FROM AgNave a WHERE a.navFrenado = :navFrenado")})
public class AgNave implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "nav_id")
    private Integer navId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nav_fabricante")
    private String navFabricante;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nav_modelo")
    private String navModelo;
    @Size(max = 2147483647)
    @Column(name = "nav_config")
    private String navConfig;
    @Size(max = 2147483647)
    @Column(name = "nav_motor")
    private String navMotor;
    @Size(max = 2147483647)
    @Column(name = "nav_estabilizador")
    private String navEstabilizador;
    @Size(max = 2147483647)
    @Column(name = "nav_frenado")
    private String navFrenado;

    public AgNave() {
    }

    public AgNave(Integer navId) {
        this.navId = navId;
    }

    public AgNave(Integer navId, String navFabricante, String navModelo) {
        this.navId = navId;
        this.navFabricante = navFabricante;
        this.navModelo = navModelo;
    }

    public Integer getNavId() {
        return navId;
    }

    public void setNavId(Integer navId) {
        this.navId = navId;
    }

    public String getNavFabricante() {
        return navFabricante;
    }

    public void setNavFabricante(String navFabricante) {
        this.navFabricante = navFabricante;
    }

    public String getNavModelo() {
        return navModelo;
    }

    public void setNavModelo(String navModelo) {
        this.navModelo = navModelo;
    }

    public String getNavConfig() {
        return navConfig;
    }

    public void setNavConfig(String navConfig) {
        this.navConfig = navConfig;
    }

    public String getNavMotor() {
        return navMotor;
    }

    public void setNavMotor(String navMotor) {
        this.navMotor = navMotor;
    }

    public String getNavEstabilizador() {
        return navEstabilizador;
    }

    public void setNavEstabilizador(String navEstabilizador) {
        this.navEstabilizador = navEstabilizador;
    }

    public String getNavFrenado() {
        return navFrenado;
    }

    public void setNavFrenado(String navFrenado) {
        this.navFrenado = navFrenado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (navId != null ? navId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgNave)) {
            return false;
        }
        AgNave other = (AgNave) object;
        if ((this.navId == null && other.navId != null) || (this.navId != null && !this.navId.equals(other.navId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.AgNave[ navId=" + navId + " ]";
    }
    
}
