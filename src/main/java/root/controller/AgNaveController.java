/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.AgNaveDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.entities.AgNave;

/**
 *
 * @author Matico
 */
@WebServlet(name = "AgNaveController", urlPatterns = { "/ag-nave-controller" })
public class AgNaveController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
    // + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AgNaveDAO dao = new AgNaveDAO();
        List<AgNave> naves = dao.findAgNaveEntities();
        request.setAttribute("naves", naves);

        request.getRequestDispatcher("lista.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AgNaveDAO dao = new AgNaveDAO();

        // CREAR
        if (request.getParameter("accion").equalsIgnoreCase("crear")) {
            String fabricante = request.getParameter("fabricante");
            String modelo = request.getParameter("modelo");
            String config = request.getParameter("config");
            String motor = request.getParameter("motor");
            String estabilizador = request.getParameter("estabilizador");
            String frenado = request.getParameter("frenado");

            AgNave nave = new AgNave();
            nave.setNavFabricante(fabricante);
            nave.setNavModelo(modelo);
            nave.setNavConfig(config);
            nave.setNavMotor(motor);
            nave.setNavEstabilizador(estabilizador);
            nave.setNavFrenado(frenado);

            try {
                dao.create(nave);
                Logger.getLogger("log").log(Level.INFO, "Valor id nave: {0}", nave.getNavId());
            } catch (Exception ex) {
                Logger.getLogger("log").log(Level.SEVERE, "Error al insertar nave. {0}", ex.getMessage());
            }

            this.doGet(request, response);

            // EDITAR
        } else if (request.getParameter("accion").equalsIgnoreCase("editar")) {
            Integer id = Integer.parseInt(request.getParameter("id"));
            AgNave nave = dao.findAgNave(id);

            request.setAttribute("nave", nave);
            request.getRequestDispatcher("editar.jsp").forward(request, response);

            // GUARDAR
        } else if (request.getParameter("accion").equalsIgnoreCase("guardar")) {
            Integer id = Integer.parseInt(request.getParameter("id"));
            String fabricante = request.getParameter("fabricante");
            String modelo = request.getParameter("modelo");
            String config = request.getParameter("config");
            String motor = request.getParameter("motor");
            String estabilizador = request.getParameter("estabilizador");
            String frenado = request.getParameter("frenado");

            AgNave nave = new AgNave();
            nave.setNavId(id);
            nave.setNavFabricante(fabricante);
            nave.setNavModelo(modelo);
            nave.setNavConfig(config);
            nave.setNavMotor(motor);
            nave.setNavEstabilizador(estabilizador);
            nave.setNavFrenado(frenado);

            try {
                dao.edit(nave);
            } catch (Exception e) {
                Logger.getLogger(AgNaveController.class.getName()).log(Level.SEVERE, null, e);
            }

            this.doGet(request, response);

            // ELIMINAR
        } else if (request.getParameter("accion").equalsIgnoreCase("eliminar")) {
            Integer id = Integer.parseInt(request.getParameter("id"));

            try {
                dao.destroy(id);
            } catch (NonexistentEntityException e) {
                Logger.getLogger(AgNaveController.class.getName()).log(Level.SEVERE, null, e);
            }

            this.doGet(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
