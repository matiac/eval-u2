<%-- Document : lista Created on : Apr 19, 2020, 6:18:53 PM Author : Matico --%>
<%@page import="java.util.List"%> <%@page import="java.util.Iterator"%> <%@page
import="root.model.entities.AgNave"%> <%@page contentType="text/html"
pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% List<AgNave>
    naves = (List<AgNave
        >) request.getAttribute("naves"); Iterator<AgNave>
            itNaves = naves.iterator(); %>

            <html>
                <head>
                    <meta
                        http-equiv="Content-Type"
                        content="text/html; charset=UTF-8"
                    />
                    <link
                        rel="stylesheet"
                        href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
                        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
                        crossorigin="anonymous"
                    />
                    <link
                        href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap"
                        rel="stylesheet"
                    />
                    <style>
                        body {
                            font-family: 'Roboto Condensed', sans-serif;
                        }
                    </style>
                    <title>AG</title>
                </head>
                <body>
                    <div class="container">
                        <div class="pb-2 mt-4 mb-2 border-bottom">
                            <h1 class="d-inline">Naves</h1>

                            <a class="btn btn-info float-right" href="index.jsp"
                                >Nueva nave</a
                            >
                        </div>

                        <table class="mt-3 table">
                            <thead>
                                <tr>
                                    <th>Fabricante</th>
                                    <th>Modelo</th>
                                    <th>Config</th>
                                    <th>Motor</th>
                                    <th>Estabilizador</th>
                                    <th>Frenado</th>
                                    <th class="text-muted">Opciones</th>
                                </tr>
                            </thead>

                            <tbody>
                                <% while (itNaves.hasNext()) { AgNave nave =
                                itNaves.next(); %>
                                <tr>
                                    <td><%= nave.getNavFabricante() %></td>
                                    <td><%= nave.getNavModelo() %></td>
                                    <td><%= nave.getNavConfig() %></td>
                                    <td><%= nave.getNavMotor() %></td>
                                    <td><%= nave.getNavEstabilizador() %></td>
                                    <td><%= nave.getNavFrenado() %></td>
                                    <td>
                                        <form
                                            action="ag-nave-controller"
                                            method="POST"
                                        >
                                            <input
                                                type="hidden"
                                                name="id"
                                                value="<%= nave.getNavId() %>"
                                            />
                                            <input
                                                class="btn btn-outline-warning btn-sm btn-block"
                                                type="submit"
                                                name="accion"
                                                value="Editar"
                                            />
                                        </form>

                                        <form
                                            action="ag-nave-controller"
                                            method="POST"
                                        >
                                            <input
                                                type="hidden"
                                                name="id"
                                                value="<%= nave.getNavId() %>"
                                            />
                                            <input
                                                class="btn btn-outline-danger btn-sm btn-block"
                                                type="submit"
                                                name="accion"
                                                value="Eliminar"
                                            />
                                        </form>
                                    </td>
                                </tr>
                                <% } %>
                            </tbody>
                        </table>
                    </div>
                </body>
            </html>
        </AgNave></AgNave
    ></AgNave
>
