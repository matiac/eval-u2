<%-- Document : index Created on : Apr 19, 2020, 4:37:25 PM Author : Matico --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
            crossorigin="anonymous"
        />
        <link
            href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap"
            rel="stylesheet"
        />
        <style>
            body {
                font-family: 'Roboto Condensed', sans-serif;
            }

            .form-group {
                margin-bottom: 1.5rem;
            }
        </style>
        <title>AG</title>
    </head>
    <body>
        <div class="container">
            <div class="pb-2 mt-4 mb-2 border-bottom">
                <h1 class="d-inline">Nueva nave</h1>

                <form
                    class="d-inline clearfix"
                    action="ag-nave-controller"
                    method="GET"
                >
                    <input
                        class="btn btn-info float-right"
                        type="submit"
                        value="Ver naves"
                    />
                </form>
            </div>

            <div class="mt-3 p-2 border">
                <form action="ag-nave-controller" method="POST">
                    <div class="form-group">
                        <label for="fabricante">Fabricante:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="fabricante"
                            required
                        />
                    </div>

                    <div class="form-group">
                        <label for="modelo">Modelo de diseño:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="modelo"
                            required
                        />
                    </div>

                    <div class="form-group">
                        <label for="config">Configuración de motor:</label>
                        <input class="form-control" type="text" name="config" />
                    </div>

                    <div class="form-group">
                        <label for="motor">Modelo de motor:</label>
                        <input class="form-control" type="text" name="motor" />
                    </div>

                    <div class="form-group">
                        <label for="estabilizador">Estabilizador:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="estabilizador"
                        />
                    </div>

                    <div class="form-group">
                        <label for="frenado">Sistema de frenado:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="frenado"
                        />
                    </div>

                    <div class="d-flex flex-row-reverse pt-2 pb-2">
                        <input
                            class="btn btn-outline-primary ml-3"
                            type="submit"
                            name="accion"
                            value="Crear"
                        />

                        <button class="btn btn-outline-secondary" type="reset">
                            Limpiar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
