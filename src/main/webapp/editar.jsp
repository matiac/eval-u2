<%-- Document : editar Created on : Apr 20, 2020, 1:17:57 PM Author : Matico
--%> <%@page import="root.model.entities.AgNave"%> <%@page
contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% AgNave nave = (AgNave) request.getAttribute("nave"); %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
            crossorigin="anonymous"
        />
        <link
            href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap"
            rel="stylesheet"
        />
        <style>
            body {
                font-family: 'Roboto Condensed', sans-serif;
            }

            .form-group {
                margin-bottom: 1.5rem;
            }
        </style>
        <title>AG</title>
    </head>
    <body>
        <div class="container">
            <div class="pb-2 mt-4 mb-2 border-bottom">
                <h1 class="d-inline">Editar nave</h1>

                <form
                    class="d-inline clearfix"
                    action="ag-nave-controller"
                    method="GET"
                >
                    <input
                        class="btn btn-info float-right"
                        type="submit"
                        value="Ver naves"
                    />
                </form>
            </div>

            <div class="mt-3 p-2 border">
                <form action="ag-nave-controller" method="POST">
                    <input
                        type="hidden"
                        name="id"
                        value="<%= nave.getNavId() %>"
                    />

                    <div class="form-group">
                        <label for="fabricante">Fabricante:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="fabricante"
                            value="<%= nave.getNavFabricante() %>"
                            required
                        />
                    </div>

                    <div class="form-group">
                        <label for="modelo">Modelo de diseño:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="modelo"
                            value="<%= nave.getNavModelo() %>"
                            required
                        />
                    </div>

                    <div class="form-group">
                        <label for="config">Configuración de motor:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="config"
                            value="<%= nave.getNavConfig() %>"
                        />
                    </div>

                    <div class="form-group">
                        <label for="motor">Modelo de motor:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="motor"
                            value="<%= nave.getNavMotor() %>"
                        />
                    </div>

                    <div class="form-group">
                        <label for="estabilizador">Estabilizador:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="estabilizador"
                            value="<%= nave.getNavEstabilizador() %>"
                        />
                    </div>

                    <div class="form-group">
                        <label for="frenado">Sistema de frenado:</label>
                        <input
                            class="form-control"
                            type="text"
                            name="frenado"
                            value="<%= nave.getNavFrenado() %>"
                        />
                    </div>

                    <div class="d-flex flex-row-reverse pt-2 pb-2">
                        <input
                            class="btn btn-outline-primary ml-3"
                            type="submit"
                            name="accion"
                            value="Guardar"
                        />

                        <button class="btn btn-outline-secondary" type="reset">
                            Restablecer
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
